package main

import (
	"fmt"
	"os"
	"time"

	shttp "net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	_ "github.com/lib/pq"
	"github.com/namsral/flag"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/http"
	"gitlab.com/latency.at/latencyAt/mailgun"
	"gitlab.com/latency.at/latencyAt/postgres"
	"gitlab.com/latency.at/latencyAt/subs"
	"gitlab.com/latency.at/latencyAt/version"
)

var (
	config = flag.String(flag.DefaultConfigFlagname, "", "Path to config file")

	listen    = flag.String("web-listen", ":8000", "Address to listen on")
	listenInt = flag.String("web-listen-int", ":8001", "Address to listen on for internal requests (reload, metrics etc)")
	assetsDir = flag.String("web-static", "static/", "Content served on /static")

	databaseURL    = flag.String("db-url", "postgres://latencyat:foobar23@localhost/latencyat", "Database URL")
	databaseDriver = flag.String("db-driver", "postgres", "Database driver")

	domain     = flag.String("domain-main", "latency.at", "Domain name to use for links")
	mailDomain = flag.String("domain-mail", "latency.at", "Domain name to use for Mailgun")

	stripeKey     = flag.String("stripe-key", "sk_test_abcd", "Stripe secret key")
	stripeHookKey = flag.String("stripe-hook-key", "whsec_test", "Stripe hook signing key")

	vatRatesFilename = flag.String("vat-file", "vat_rates.csv", "Path to vat_rates.csv file")
	logLevelName     = flag.String("log-level", "info", "Only log this severity and above.")
	jwtSigningKey    = flag.String("jwt-key", "some-random-string-please-change-me", "JWT Signing key")
	printVersion     = flag.Bool("v", false, "Print version and exit")
	disableMail      = flag.Bool("mail-disable", false, "Disable sending of emails")

	authUsername = flag.String("auth-user", "admin", "Basic auth username for internal endpoints like /api/book")
	authPassword = flag.String("auth-pass", "foobar23", "Basic Auth password")
)

func main() {
	flag.Parse()
	ll, err := log.ParseLevel(*logLevelName)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(ll)
	if *printVersion {
		fmt.Println(version.String())
		os.Exit(0)
	}

	db, err := sqlx.Open(*databaseDriver, *databaseURL)
	if err != nil {
		log.Fatal(err)
	}

	// Create tables if not exist
	if _, err := db.Exec(postgres.EmailTokenServiceSchema +
		postgres.UserServiceSchema +
		postgres.TokenServiceSchema +
		postgres.EventServiceSchema); err != nil {
		log.Fatal("Couldn't create tables:", err)
	}

	defer db.Close()
	ms, err := mailgun.NewMailService(*domain, *mailDomain, "key-7b408dfa75e9da1bf2c7ac376d4b6af5", "Latency.at <support+replies@latency.at>", "templates", *disableMail)
	if err != nil {
		log.Fatal(err)
	}

	ta := &http.TokenAuth{
		JWTSigningKey:    []byte(*jwtSigningKey),
		JWTSigningMethod: jwt.SigningMethodHS256,
	}

	// database services
	us, err := postgres.NewUserService(db)
	if err != nil {
		log.Fatal(err)
	}
	ets, err := postgres.NewEmailTokenService(db)
	if err != nil {
		log.Fatal(err)
	}
	ts, err := postgres.NewTokenService(db)
	if err != nil {
		log.Fatal(err)
	}
	es, err := postgres.NewEventService(db)
	if err != nil {
		log.Fatal(err)
	}

	// external services
	ss, err := subs.NewSubscriptionService(us, es, *vatRatesFilename)
	if err != nil {
		log.Fatal(err)
	}

	conf := latencyAt.DefaultConfig
	conf.StripeHookSigningKey = *stripeHookKey
	conf.AuthUsername = *authUsername
	conf.AuthPassword = *authPassword
	ah, err := http.NewAPIHandler(latencyAt.DefaultConfig, us, ms, ets, ta, ts, ss, *stripeKey)
	if err != nil {
		log.Fatal(err)
	}

	connectionGauge := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "lat",
			Subsystem: "postgres",
			Name:      "connections_open",
			Help:      "Current number of open connections to postgres",
		},
	)
	prometheus.MustRegister(connectionGauge)

	intMux := shttp.NewServeMux()
	intMux.HandleFunc("/health", ah.HandleHealth)
	intMux.HandleFunc("/metrics", func(w shttp.ResponseWriter, r *shttp.Request) {
		stats := db.Stats()
		connectionGauge.Set(float64(stats.OpenConnections))
		promhttp.Handler().ServeHTTP(w, r)
	})
	intServer := &shttp.Server{
		Addr:           *listenInt,
		Handler:        intMux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	go func() {
		log.Println("Serving prometheus metrics on", *listenInt)
		log.Fatal(intServer.ListenAndServe())
	}()

	server := http.NewServer()
	server.Addr = *listen
	server.Handler = &http.Handler{
		APIHandler:  ah,
		FileHandler: http.NewFileHandler(*assetsDir),
	}
	log.Println("Serving API on", *listen)
	if err := server.Open(); err != nil {
		log.Fatal(err)
	}
}
