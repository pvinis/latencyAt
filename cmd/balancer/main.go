package main

import (
	"log"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/latency.at/latencyAt"

	_ "github.com/lib/pq"
	"github.com/namsral/flag"
)

const updateBalanceFreePlan = `UPDATE users
  SET
    balance = $1,
    last_billing = now()
  WHERE
    stripe_customer_id = '' AND
    balance < $1 AND
    (last_billing < (now() - interval '1 month') OR
      registered < (now() - interval '1 month'))`

var (
	config = flag.String(flag.DefaultConfigFlagname, "", "Path to config file")

	databaseURL    = flag.String("db-url", "postgres://latencyat:foobar23@localhost/latencyat", "Database URL")
	databaseDriver = flag.String("db-driver", "postgres", "Database driver")
	interval       = flag.Duration("interval", 10*time.Minute, "Balancer interval")
	metricsAddr    = flag.String("metrics-addr", ":8080", "Address to serve metrics on")

	cycleCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "balancer",
			Name:      "cycles_total",
			Help:      "Total number of balancer cycles",
		},
	)
	updateCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "balancer",
			Name:      "updates_total",
			Help:      "Total number of updates by balancer",
		},
	)
)

func main() {
	flag.Parse()
	db, err := sqlx.Open(*databaseDriver, *databaseURL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	stmt, err := db.Preparex(updateBalanceFreePlan)
	if err != nil {
		log.Fatal(err)
	}

	prometheus.MustRegister(cycleCounter)
	prometheus.MustRegister(updateCounter)
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		log.Fatal(http.ListenAndServe(*metricsAddr, nil))
	}()

	for {
		cycleCounter.Inc()
		res, err := stmt.Exec(latencyAt.DefaultBalance)
		if err != nil {
			log.Fatal(err)
		}
		n, err := res.RowsAffected()
		if err != nil {
			log.Println(err)
		}
		updateCounter.Add(float64(n))
		log.Println(n, "rows updated")
		time.Sleep(*interval)
	}
}
