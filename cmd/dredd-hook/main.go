package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"log"
	"strconv"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/postgres"

	"github.com/snikch/goodman/hooks"
	"github.com/snikch/goodman/transaction"
)

var (
	databaseURL    = flag.String("d", "postgres://latencyat:foobar23@localhost/latencyat", "Database URL")
	databaseDriver = flag.String("dd", "postgres", "Database driver")
)

func main() {
	db, err := sqlx.Open(*databaseDriver, *databaseURL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	us, err := postgres.NewUserService(db)
	if err != nil {
		log.Fatal(err)
	}
	es, err := postgres.NewEmailTokenService(db)
	if err != nil {
		log.Fatal(err)
	}

	h := hooks.NewHooks()
	server := hooks.NewServer(h)

	user := &latencyAt.User{}
	h.Before(
		"/api/user > Create new user > 201 > application/json",
		func(t *transaction.Transaction) {
			var err error
			user, err = latencyAt.UserFromJSON(bytes.NewBufferString(t.Request.Body))
			log.Println("user new: ", user)
			if err != nil {
				log.Fatal(err)
			}
		},
	)
	h.Before(
		"/api/user/activate/{token} > Activate user identified by token > 200 > application/json",
		func(t *transaction.Transaction) {
			ruser, err := us.UserByEmail(user.Email)
			if err != nil {
				log.Fatal(err)
			}
			token, err := es.EmailToken(*ruser.EmailTokenID)
			if err != nil {
				log.Fatal(err)
			}
			log.Println("current uri: ", t.Request.URI)
			t.FullPath = "/api/user/activate/" + token.Token
		},
	)

	h.Before(
		"/api/user/login > Return JWT to be used for authenticating against API > 200 > application/json",
		func(t *transaction.Transaction) {
			log.Println("user: ", user)
			b, err := json.Marshal(user)
			if err != nil {
				log.Fatal(err)
			}
			t.Request.Body = string(b)
		},
	)

	token := ""
	h.After(
		"/api/user/login > Return JWT to be used for authenticating against API > 200 > application/json",
		func(t *transaction.Transaction) {
			type tokenS struct {
				Token string `json:"token"`
			}
			tt := &tokenS{}
			if err := json.Unmarshal([]byte(t.Real.Body), tt); err != nil {
				log.Fatal(err)
			}
			token = tt.Token
		},
	)
	h.BeforeEach(func(t *transaction.Transaction) {
		if _, ok := t.Request.Headers["Authorization"]; ok {
			if token == "" {
				log.Fatal("Auth header but not token")
			}
			log.Println("old/fake auth: ", t.Request.Headers["Authorization"])
			log.Println("fixing auth to: ", token)
			t.Request.Headers["Authorization"] = "Bearer " + token
			log.Println(t.Request.Headers["Authorization"])
		}
	})

	promTokenID := 0
	h.After(
		"/api/tokens > Return tokens for current user > 200 > application/json",
		func(t *transaction.Transaction) {
			tokens := map[string][]latencyAt.Token{}
			if err := json.Unmarshal([]byte(t.Real.Body), &tokens); err != nil {
				log.Fatal(err)
			}
			log.Println("tokens: ", tokens)
			promTokenID = tokens["tokens"][0].ID
		},
	)
	h.Before(
		"/api/tokens/{id} > Delete token with given id > 200 > application/json",
		func(t *transaction.Transaction) {
			log.Println("actually deleting", promTokenID)
			t.FullPath = "/api/tokens/" + strconv.Itoa(promTokenID)
		},
	)
	server.Serve()
	defer server.Listener.Close()
}

/*h.Before("/api/user/activate/{token} > Activate user identified by token > 201 > application/json", func(t *transaction.Transaction) {
	parts := strings.Split(t.Request.URI, "/")
	token := &latencyAt.EmailToken{Token: parts[len(parts)-1]}
	if err := es.CreateEmailToken(token); err != nil {
		log.Fatal(err)
	}
	if err := us.Create(&latencyAt.User{Email: "foo@example.com", EmailTokenID: &token.ID}); err != nil {
		log.Fatal(err)
	}
})*/
