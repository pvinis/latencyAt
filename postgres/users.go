package postgres

import (
	"github.com/jmoiron/sqlx"

	log "github.com/Sirupsen/logrus"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

// Ensure TokenService implements latencyAt.TokenService.
var (
	_ latencyAt.UserService = &UserService{}
)

const UserServiceSchema = `
CREATE TABLE IF NOT EXISTS users (
  id BIGSERIAL PRIMARY KEY,
  email CITEXT UNIQUE,
  email_token_id int8 REFERENCES email_token (id),
  password text,
  activated bool default false,
  registered timestamp,
  stripe_customer_id varchar(255) DEFAULT '',
  balance integer,
  requests_month integer,
  last_billing timestamp,
  vatin varchar(255) DEFAULT ''
);
`

type UserService struct {
	db                           *sqlx.DB
	stmtSelectByID               *sqlx.Stmt
	stmtSelectByEmail            *sqlx.Stmt
	stmtSelectByStripeCustomerID *sqlx.Stmt
	stmtSelectByEmailTokenID     *sqlx.Stmt

	stmtInsert    *sqlx.Stmt
	stmtDeleteAll *sqlx.Stmt

	stmtSetActivated          *sqlx.Stmt
	stmtSetPasswordHash       *sqlx.Stmt
	stmtSetEmailAndDeactivate *sqlx.Stmt

	stmtSetStripeCustomerID             *sqlx.Stmt
	stmtSetVatIn                        *sqlx.Stmt
	stmtSetBalance                      *sqlx.Stmt
	stmtSetBalanceAbs                   *sqlx.Stmt
	stmtSetBalanceAbsByStripeCustomerID *sqlx.Stmt
	stmtSetBalanceByStripeCustomerID    *sqlx.Stmt

	stmtIncRequests        *sqlx.Stmt
	stmtUnsetEmailTokenRef *sqlx.Stmt

	stmtDeleteEmailToken *sqlx.Stmt // Move to EmailTokenService?
}

func NewUserService(db *sqlx.DB) (*UserService, error) {
	us := &UserService{db: db}

	for ref, str := range map[**sqlx.Stmt]string{
		&us.stmtSelectByID:               "SELECT id,         email, email_token_id, password, activated, registered, stripe_customer_id, balance, requests_month, last_billing, vatin FROM users where id = $1",
		&us.stmtSelectByEmail:            "SELECT id,         email, email_token_id, password, activated, registered, stripe_customer_id, balance, requests_month, last_billing, vatin FROM users where email = $1",
		&us.stmtSelectByStripeCustomerID: "SELECT id,         email, email_token_id, password, activated, registered, stripe_customer_id, balance, requests_month, last_billing, vatin FROM users where stripe_customer_id = $1",
		&us.stmtSelectByEmailTokenID:     "SELECT id,         email, email_token_id, password, activated, registered, stripe_customer_id, balance, requests_month, last_billing, vatin FROM users WHERE email_token_id = $1",
		&us.stmtInsert:                   "INSERT INTO users (email, email_token_id, password, activated, registered,                     balance, requests_month, last_billing, vatin) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id",

		&us.stmtSetActivated:          "UPDATE users SET activated = true WHERE id = $1",
		&us.stmtSetPasswordHash:       "UPDATE users SET password = $2 WHERE id = $1",
		&us.stmtSetEmailAndDeactivate: "UPDATE users SET email = $2, activated = false WHERE id = $1",
		&us.stmtDeleteEmailToken:      "DELETE FROM email_token USING users WHERE email_token.id = users.email_token_id AND users.id = $1",
		&us.stmtUnsetEmailTokenRef:    "UPDATE users SET email_token_id = NULL where id = $1",
		&us.stmtSetStripeCustomerID:   "UPDATE users SET stripe_customer_id = $2 WHERE id = $1",
		&us.stmtSetVatIn:              "UPDATE users SET vatin = $2 WHERE id = $1",

		&us.stmtSetBalance:                      "UPDATE users SET balance = balance + $2 WHERE id = $1",
		&us.stmtSetBalanceAbs:                   "UPDATE users SET balance =           $2 WHERE id = $1",
		&us.stmtSetBalanceAbsByStripeCustomerID: "UPDATE users SET balance =           $2 WHERE stripe_customer_id = $1",
		&us.stmtSetBalanceByStripeCustomerID:    "UPDATE users SET balance = balance + $2 WHERE stripe_customer_id = $1",
		&us.stmtIncRequests:                     "UPDATE users SET requests_month = requests_month + $2 WHERE id = $1",

		&us.stmtDeleteAll: "DELETE FROM users",
	} {
		s, err := db.Preparex(str)
		if err != nil {
			return nil, err
		}
		*ref = s
	}
	return us, nil
}

func (s *UserService) Healthy() error {
	return s.db.Ping()
}

func (s *UserService) UserByEmail(email string) (*latencyAt.User, error) {
	rows, err := s.stmtSelectByEmail.Query(email)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, errors.ErrUserNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	user := &latencyAt.User{}
	return user, rows.Scan(&user.ID, &user.Email, &user.EmailTokenID, &user.PasswordHash,
		&user.Activated, &user.Registered, &user.StripeCustomerID, &user.Balance, &user.RequestsMonth, &user.LastBilling, &user.VatIn)
}

func (s *UserService) User(id int) (*latencyAt.User, error) {
	rows, err := s.stmtSelectByID.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, errors.ErrUserNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	user := &latencyAt.User{ID: id}
	return user, rows.Scan(&user.ID, &user.Email, &user.EmailTokenID, &user.PasswordHash,
		&user.Activated, &user.Registered, &user.StripeCustomerID, &user.Balance, &user.RequestsMonth, &user.LastBilling, &user.VatIn)
}

func (s *UserService) UserByEmailTokenID(id int) (*latencyAt.User, error) {
	rows, err := s.stmtSelectByEmailTokenID.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, errors.ErrUserNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	user := &latencyAt.User{}
	return user, rows.Scan(&user.ID, &user.Email, &user.EmailTokenID, &user.PasswordHash,
		&user.Activated, &user.Registered, &user.StripeCustomerID, &user.Balance, &user.RequestsMonth, &user.LastBilling, &user.VatIn)
}

func (s *UserService) UserByStripeCustomerID(id string) (*latencyAt.User, error) {
	rows, err := s.stmtSelectByStripeCustomerID.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if !rows.Next() {
		return nil, errors.ErrUserNotFound
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	user := &latencyAt.User{}
	return user, rows.Scan(&user.ID, &user.Email, &user.EmailTokenID, &user.PasswordHash,
		&user.Activated, &user.Registered, &user.StripeCustomerID, &user.Balance, &user.RequestsMonth, &user.LastBilling, &user.VatIn)
}

// Create inserts a new user into the database
func (s *UserService) Create(u *latencyAt.User) error {
	if u.ID != 0 {
		return errors.ErrCreateWithID
	}
	return s.stmtInsert.QueryRow(
		u.Email,
		u.EmailTokenID,
		u.PasswordHash,
		u.Activated,
		u.Registered,
		u.Balance,
		u.RequestsMonth,
		u.LastBilling,
		u.VatIn,
	).Scan(&u.ID)
}

func (s *UserService) Activate(u *latencyAt.User) error {
	_, err := s.stmtSetActivated.Exec(u.ID)
	return err
}

func (s *UserService) SetPasswordHash(id int, passwordHash []byte) error {
	tx, err := s.db.Beginx()
	if err != nil {
		return err
	}
	_, err = tx.Stmtx(s.stmtSetPasswordHash).Exec(id, passwordHash)
	if err != nil {
		if err := tx.Rollback(); err != nil {
			log.Warn("Couldn't rollback:", err)
		}
		return err
	}
	_, err = tx.Stmtx(s.stmtUnsetEmailTokenRef).Exec(id)
	if err != nil {
		if err := tx.Rollback(); err != nil {
			log.Warn("Couldn't rollback:", err)
		}
		return err
	}
	_, err = tx.Stmtx(s.stmtDeleteEmailToken).Exec(id)
	if err != nil {
		if err := tx.Rollback(); err != nil {
			log.Warn("Couldn't rollback:", err)
		}
		return err
	}
	return tx.Commit()
}

func (s *UserService) SetEmailAndDeactivate(id int, email string) error {
	_, err := s.stmtSetEmailAndDeactivate.Exec(id, email)
	return err
}

func (s *UserService) SetStripeCustomerID(id int, customerID string) error {
	_, err := s.stmtSetStripeCustomerID.Exec(id, customerID)
	return err
}

func (s *UserService) SetVatIn(uid int, vatIn string) error {
	_, err := s.stmtSetVatIn.Exec(uid, vatIn)
	return err
}

// Balance *increments* the balance and has a shitty name.
func (s *UserService) Balance(user *latencyAt.User, balance int) error {
	_, err := s.stmtSetBalance.Exec(user.ID, balance)
	return err
}

func (s *UserService) BalanceAbs(user *latencyAt.User, balance int) error {
	_, err := s.stmtSetBalanceAbs.Exec(user.ID, balance)
	return err
}

func (s *UserService) BalanceAbsByStripeCustomerID(id string, balance int) error {
	_, err := s.stmtSetBalanceAbsByStripeCustomerID.Exec(id, balance)
	return err
}

func (s *UserService) BalanceByStripeCustomerID(id string, balance int) error {
	_, err := s.stmtSetBalanceByStripeCustomerID.Exec(id, balance)
	return err
}

func (s *UserService) IncRequests(id int, requests int) error {
	_, err := s.stmtIncRequests.Exec(id, requests)
	return err
}

func (s *UserService) Purge() error {
	_, err := s.stmtDeleteAll.Exec()
	return err
}
