package latencyAt

import (
	stripe "github.com/stripe/stripe-go"
)

type SubscriptionService interface {
	Subscribe(userID int, planName string) error
	Unsubscribe(userID int) error
	Plans() (map[string]*stripe.Plan, error)
	ProcessPayment(invoice *stripe.Invoice) error
	GetSubscription(customerID string) (*stripe.Sub, error)
	HandleEvent(event *stripe.Event) error
	AddCard(userID int, token string) error
	GetCard(userID int) (*stripe.Card, error)
	DeleteCard(userID int) error
	GetTax(country string) (*VatRate, error)
}
