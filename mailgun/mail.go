package mailgun

import (
	"bytes"
	"html/template"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"

	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/latency.at/latencyAt"

	"gopkg.in/mailgun/mailgun-go.v1"
)

var (
	// Ensure EmailTokenService implements latencyAt.EmailTokenService.
	_ latencyAt.MailService = &MailService{}

	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "mailgun",
			Name:      "requests_total",
			Help:      "Total number of requests by function",
		},
		[]string{"function"},
	)

	requestDuration = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "lat",
			Subsystem: "mailgun",
			Name:      "request_duration_seconds",
			Help:      "Duration of last request send to mailgun API by function",
		},
		[]string{"function"},
	)

	errorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "mailgun",
			Name:      "errors_total",
			Help:      "Total number of error in mailgun package by function",
		},
		[]string{"function"},
	)
)

func init() {
	prometheus.MustRegister(requestDuration)
	prometheus.MustRegister(errorCounter)
}

type mailContent struct {
	Title      string
	Intro      string
	Body       string
	URL        string
	ButtonText string
}

type mail struct {
	*mailContent
	domain       string
	from         string
	path         string
	htmlTemplate *template.Template
	textTemplate *template.Template
}

func (m *mail) send(mg mailgun.Mailgun, user *latencyAt.User, emailToken *latencyAt.EmailToken) error {
	content := &mailContent{
		Title:      m.mailContent.Title,
		Intro:      m.mailContent.Intro,
		Body:       m.mailContent.Body,
		ButtonText: m.mailContent.ButtonText,
		URL:        strings.Join([]string{"https:/", m.domain, m.path, emailToken.Token}, "/"),
	}
	html := &bytes.Buffer{}
	if err := m.htmlTemplate.Execute(html, content); err != nil {
		return err
	}
	text := &bytes.Buffer{}
	if err := m.textTemplate.Execute(text, content); err != nil {
		return err
	}

	message := mg.NewMessage(m.from, content.Title, text.String(), user.Email)
	message.SetHtml(html.String())
	resp, id, err := mg.Send(message)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"id": id,
		"to": user.Email,
	}).Infoln("Successfully send mail:", resp)
	return nil
}

func NewMailService(domain, mailDomain, apiKey, from, templateDir string, disabled bool) (*MailService, error) {
	htmlTemplate, err := template.ParseFiles(templateDir + "/mail.html.tmpl")
	if err != nil {
		return nil, err
	}

	textTemplate, err := template.ParseFiles(templateDir + "/mail.text.tmpl")
	if err != nil {
		return nil, err
	}

	ms := &MailService{
		Mailgun: mailgun.NewMailgun(mailDomain, apiKey, ""),
		activationMail: &mail{
			htmlTemplate: htmlTemplate,
			textTemplate: textTemplate,
			mailContent: &mailContent{
				Title:      "Confirm your Email address",
				Intro:      "Welcome to Latency.at!",
				Body:       "",
				ButtonText: "Activate Account",
			},
			domain: domain,
			path:   "activate",
			from:   from,
		},
		resetMail: &mail{
			htmlTemplate: htmlTemplate,
			textTemplate: textTemplate,
			mailContent: &mailContent{
				Title:      "Reset your password",
				Intro:      "Somebody requested to reset your password on latency.at.",
				Body:       "If you want to keep your current password, please ignore this email. To change it, open the link below:",
				ButtonText: "Reset Password",
			},
			domain: domain,
			path:   "reset",
			from:   from,
		},
		disabled: disabled,
	}
	return ms, nil
}

type MailService struct {
	mailgun.Mailgun
	activationMail *mail
	resetMail      *mail
	disabled       bool
}

func (s *MailService) Healthy() error {
	requestCounter.WithLabelValues("Healthy").Inc()
	start := time.Now()

	_, _, _, err := s.Mailgun.GetSingleDomain(s.Mailgun.Domain())
	if err != nil {
		errorCounter.WithLabelValues("Healthy").Inc()
		return err
	}
	requestDuration.WithLabelValues("Healthy").Set(time.Since(start).Seconds())
	return nil
}

func (s *MailService) SendActivation(user *latencyAt.User, emailToken *latencyAt.EmailToken) error {
	if s.disabled {
		log.Println("Mail disabled, not sending activation email")
		return nil
	}
	requestCounter.WithLabelValues("SendActivation").Inc()
	start := time.Now()
	if err := s.activationMail.send(s.Mailgun, user, emailToken); err != nil {
		errorCounter.WithLabelValues("SendActivation").Inc()
		return err
	}
	requestDuration.WithLabelValues("SendActivation").Set(time.Since(start).Seconds())
	return nil
}

func (s *MailService) SendReset(user *latencyAt.User, emailToken *latencyAt.EmailToken) error {
	requestCounter.WithLabelValues("SendReset").Inc()
	start := time.Now()
	if s.disabled {
		log.Println("Mail disabled, not sending reset email")
		return nil
	}
	if err := s.resetMail.send(s.Mailgun, user, emailToken); err != nil {
		errorCounter.WithLabelValues("SendReset").Inc()
		return err
	}
	requestDuration.WithLabelValues("SendReset").Set(time.Since(start).Seconds())
	return nil
}
