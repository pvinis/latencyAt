package http

import (
	"net/http"
	"strconv"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/latency.at/latencyAt"
)

func (a *APIHandler) handleGetTokens(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	tokens, err := a.TokenService.TokensByUserID(userID)
	if err != nil {
		return nil, err
	}
	return map[string][]latencyAt.Token{"tokens": tokens}, nil
}

func (a *APIHandler) handleCreateToken(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	t, err := genToken(a.Config.TokenLength)
	if err != nil {
		return nil, err
	}
	if err := a.TokenService.CreateToken(&latencyAt.Token{
		Token:   t,
		UserID:  userID,
		Created: time.Now(),
	}); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleDeleteToken(r *http.Request, ps httprouter.Params, userID int) (interface{}, error) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		return nil, err
	}
	if err := a.TokenService.DeleteToken(&latencyAt.Token{
		ID:     id,
		UserID: userID,
	}); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}
