package http

import "net/http"

func NewFileHandler(staticDir string) http.Handler {
	return http.FileServer(http.Dir(staticDir))
}
