package http_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	shttp "net/http"
	"net/url"
	"reflect"
	"testing"
	"time"

	"golang.org/x/crypto/bcrypt"

	jwt "github.com/dgrijalva/jwt-go"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
	"gitlab.com/latency.at/latencyAt/http"
	"gitlab.com/latency.at/latencyAt/mock"
)

const (
	stripeKey = "pk_foobar"
)

func testSignup(t *testing.T, userPost *latencyAt.User) (*shttp.Response, error) {
	us := &mock.UserService{
		CreateFn: func(u *latencyAt.User) error {
			if u.Email != userPost.Email {
				t.Fatalf("Email mismatch: %s != %s", u.Email,
					userPost.Email)
			}
			if err := bcrypt.CompareHashAndPassword(
				[]byte(u.PasswordHash),
				[]byte(u.Password),
			); err != nil {
				t.Fatal("Created hash and password don't match:", err)
			}
			if err := bcrypt.CompareHashAndPassword(
				[]byte(u.PasswordHash),
				[]byte(userPost.Password),
			); err != nil {
				t.Fatal(err)
			}
			return nil
		},
		UserByEmailFn: func(email string) (*latencyAt.User, error) {
			if email != userPost.Email {
				t.Fatalf("Email mismatch: %s != %s", email,
					userPost.Email)
			}
			return nil, errors.ErrUserNotFound
		},
	}
	etid := 23
	es := &mock.EmailTokenService{
		CreateEmailTokenFn: func(et *latencyAt.EmailToken) error {
			if len(et.Token) != latencyAt.DefaultConfig.EmailTokenLength {
				t.Fatal("Expected token to be 12 characters")
			}
			et.ID = etid
			return nil
		},
	}
	ms := &mock.MailService{
		SendActivationFn: func(u *latencyAt.User, et *latencyAt.EmailToken) error {
			if *u.EmailTokenID != etid {
				t.Fatalf("EmailTokenID mismatch: %d != %d", etid, u.EmailTokenID)
			}
			return nil
		},
	}

	ta := &http.TokenAuth{
		JWTSigningKey:    []byte("JWT-SIGNING-KEY"),
		JWTSigningMethod: jwt.SigningMethodHS256,
	}

	ah, err := http.NewAPIHandler(latencyAt.DefaultConfig, us, ms, es, ta, nil, nil, stripeKey)
	if err != nil {
		t.Fatal(err)
	}

	server := http.NewServer()
	server.Addr = ":0"
	server.Handler = &http.Handler{
		APIHandler: ah,
	}
	go func() { t.Fatal(server.Open()) }()
	time.Sleep(1 * time.Second) // FIXME
	url := url.URL{
		Scheme: "http",
		Host:   fmt.Sprintf("localhost:%d", server.Port()),
	}

	jsonb, err := json.Marshal(userPost)
	if err != nil {
		t.Fatal(err)
	}
	return shttp.Post(url.String()+"/api/user", "text/json", bytes.NewBuffer(jsonb))
}

type response struct {
	StatusCode int
	status     *latencyAt.Status
}

func TestSignup(t *testing.T) {
	for post, expected := range map[*latencyAt.User]response{
		// valid login
		&latencyAt.User{
			Email:    "test@example.com",
			Password: "foobar23",
		}: response{
			shttp.StatusOK,
			nil,
		},

		// invalid email
		&latencyAt.User{
			Email:    "test",
			Password: "foobar23",
		}: response{
			shttp.StatusBadRequest,
			&latencyAt.Status{
				Status:  "error",
				Message: "mail: no angle-addr",
			},
		},

		// invalid password
		&latencyAt.User{
			Email:    "test@example.com",
			Password: "foo",
		}: response{
			shttp.StatusBadRequest,
			&latencyAt.Status{
				Status:  "error",
				Message: "Password too short",
			},
		},
	} {
		resp, err := testSignup(t, post)
		if err != nil {
			t.Fatal(err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != expected.StatusCode {
			t.Fatalf("Unexpected status code. Got %d, expected %d for %#v", resp.StatusCode, expected.StatusCode, post)
		}
		if expected.status == nil {
			continue
		}

		status := latencyAt.Status{}
		if err := json.NewDecoder(resp.Body).Decode(&status); err != nil {
			dat, _ := ioutil.ReadAll(resp.Body)
			t.Fatalf("Error %s decoding: %s", err, dat)
		}
		if !reflect.DeepEqual(status, *expected.status) {
			t.Fatalf("Unexpected response: %#v != %#v", status, expected.status)
		}
	}
}
