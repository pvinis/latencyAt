package mock

import "gitlab.com/latency.at/latencyAt"

type MailService struct {
	SendActivationFn func(u *latencyAt.User, t *latencyAt.EmailToken) error
}

func (s *MailService) SendActivation(u *latencyAt.User, t *latencyAt.EmailToken) error {
	return s.SendActivationFn(u, t)
}

func (s *MailService) SendReset(u *latencyAt.User, t *latencyAt.EmailToken) error {
	panic("not implemented")
}

func (s *MailService) Healthy() error {
	return nil
}
