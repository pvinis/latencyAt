package version

import "runtime"

var (
	Revision  string
	Branch    string
	Date      string
	GoVersion = runtime.Version()
)

func String() string {
	return "rev " + Revision + " (branch: " + Branch + "), built on " + Date + "with " + GoVersion
}
