FROM golang:1.9

RUN apt-get -qy update \
      && apt-get -qy install make gettext rsync \
      && go get -u github.com/kardianos/govendor

WORKDIR /go/src/gitlab.com/latency.at/latencyAt

COPY vendor/ vendor/
RUN  govendor sync

COPY .  .
RUN make build
